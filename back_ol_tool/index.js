const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
require("dotenv").config();
const resourceRoutes = require("./src/routes/resources.routes");
const fileRoutes = require("./src/routes/files.routes");
const learningObjectRoutes = require("./src/routes/learningObject.routes");
const linkedListRoutes = require("./src/routes/linkedList.routes");
const app = express();
const port = process.env.PORT || 5000;
//enable cors
app.use(cors());

//my first middleware
app.use(express.json());

app.use("/public/uploads", express.static(__dirname + "/public/uploads"));

app.get("/", (req, res) => {
  res.send("The amazing authoring tool!");
});
app.use("/api/resource", resourceRoutes);
app.use("/api/files", fileRoutes);
app.use("/api/learningObject", learningObjectRoutes);
app.use("/api/linkedList", linkedListRoutes);

mongoose
  .connect(process.env.DB)
  .then(() => console.log("Conectado con MongoDB Atlas exitosamente!!"))
  .catch((error) => console.log(error));

app.listen(port, () => {
  console.log("Server started on port: ", port);
});
