const { body, validationResult } = require("express-validator");
var async = require("async");
const cloudinary = require("../utilities/cloudinary");
const upload = require("../utilities/multer");

exports.Resource_create_file = [
  upload.single("file"),
  async (req, res, next) => {
    try {
      // Upload image to cloudinary
      const result = await cloudinary.uploader.upload(req.body.file);

      return res.status(200).json({
        message: "Image uploaded successfully",
        file: {
          path: result.secure_url,
          cloudinary_id: result.public_id,
        },
      });
    } catch (err) {
      return res.status(500).json({
        message: "Error uploading file",
        error: err,
      });
    }
  },
];

exports.Resource_update_file = [
  upload.single("file"),
  async (req, res, next) => {
    //update file
    try {
      await cloudinary.uploader.destroy(req.params.fileId);
      const result = await cloudinary.uploader.upload(req.body.file);

      return res.status(200).json({
        message: "Image updated successfully",
        file: {
          path: result.secure_url,
          cloudinary_id: result.public_id,
        },
      });
    } catch (err) {
      return res.status(500).json({
        message: "Error updating file",
        error: err,
      });
    }
  },
];

exports.Resource_delete_file = [
  upload.single("file"),
  async (req, res, next) => {
    //update file
    try {
      await cloudinary.uploader.destroy(req.params.fileId);
      return res.status(200).json({
        message: "Image deleted successfully",
        file: {
          cloudinary_id: req.params.fileId,
        },
      });
    } catch (err) {
      return res.status(500).json({
        message: "Error deleting file",
        error: err,
      });
    }
  },
];
