var LearningObject = require("../models/learningObject.model");
const { body, validationResult } = require("express-validator");

exports.LearningObject_get_all = [
  (req, res, next) => {
    LearningObject.find({}, (err, learningObjects) => {
      if (err) {
        res.status(500).json({
          error: err,
        });
      }
      res.status(200).json(learningObjects);
    });
  },
];

exports.LearningObject_get_by_id = [
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    LearningObject.findById(req.params.LearningObjectId, (err, data) => {
      if (err) {
        res.status(500).json({
          error: err,
        });
      }
      res.status(200).json({
        data,
      });
    });
  },
];

//create new LearningObject

exports.LearningObject_create = [
  //validate the request
  body("title", "Title is required").notEmpty(),
  body("abstract", "Abstract is required").notEmpty(),
  body("knowledgeArea", "Knowledge Area is required").notEmpty(),

  body("author", "Author is required").notEmpty(),
  body("degree", "Degree is required").notEmpty(),

  body("learningModel", "Learning Model is required").notEmpty(),
  body("license", "License is required").notEmpty(),
  body("keywords", "Keywords is required").notEmpty(),
  body("content", "Content is required").notEmpty(),
  //process the request
  (req, res, next) => {
    console.log("create");
    // Extract the validation errors from a request.
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    //create a new learningObject
    const learningObject = new LearningObject({
      title: req.body.title,
      abstract: req.body.abstract,
      knowledgeArea: req.body.knowledgeArea,
      relationKnowledgeArea: req.body.relationKnowledgeArea,
      author: req.body.author,
      degree: req.body.degree,
      criteriaInclution: req.body.criteriaInclution,
      learningModel: req.body.learningModel,
      license: req.body.license,
      keywords: req.body.keywords,
      content: req.body.content,
    });
    //save the learningObject
    learningObject.save((err, data) => {
      if (err) {
        return next(err);
      }
      res.status(200).json({
        data,
      });
    });
  },
];

//update LearningObject
exports.LearningObject_update_by_id = [
  //validate the request
  body("title", "Title is required").notEmpty(),
  body("abstract", "Abstract is required").notEmpty(),
  body("knowledgeArea", "Knowledge Area is required").notEmpty(),

  body("author", "Author is required").notEmpty(),
  body("degree", "Degree is required").notEmpty(),

  body("learningModel", "Learning Model is required").notEmpty(),
  body("license", "License is required").notEmpty(),
  body("keywords", "Keywords is required").notEmpty(),
  body("content", "Content is required").notEmpty(),
  //process the request
  (req, res, next) => {
    console.log("update");
    // Extract the validation errors from a request.
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    //update the learningObject
    LearningObject.findByIdAndUpdate(
      req.params.LearningObjectId,
      {
        title: req.body.title,
        abstract: req.body.abstract,
        knowledgeArea: req.body.knowledgeArea,
        relationKnowledgeArea: req.body.relationKnowledgeArea,
        author: req.body.author,
        degree: req.body.degree,
        criteriaInclution: req.body.criteriaInclution,
        learningModel: req.body.learningModel,
        license: req.body.license,
        keywords: req.body.keywords,
        content: req.body.content,
      },
      (err, data) => {
        if (err) {
          return next(err);
        }
        res.status(200).json({
          data,
        });
      }
    );
  },
];

//delete LearningObject
exports.LearningObject_delete_by_id = [
  //process the request
  (req, res, next) => {
    console.log("delete");
    //delete the learningObject
    LearningObject.findByIdAndRemove(
      req.params.LearningObjectId,
      (err, data) => {
        if (err) {
          res.status(500).json({
            error: err,
          });
        }
        res.status(200).json({
          data,
        });
      }
    );
  },
];
