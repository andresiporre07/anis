var LinkedList = require("../models/linkedList.model");
const { body, validationResult } = require("express-validator");

//get all linked lists
exports.get_all_linked_list = [
  (req, res, next) => {
    LinkedList.find({}, (err, linkedLists) => {
      if (err) {
        res.status(500).json({
          error: err,
        });
      }
      res.status(200).json(linkedLists);
    });
  },
];

//get a linked list by id
exports.get_linked_list_by_id = [
  //get by id
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    LinkedList.findById(req.params.LinkedListId, (err, data) => {
      if (err) {
        res.status(500).json({
          error: err,
        });
      }
      res.status(200).json({
        data,
      });
    });
  },
];

//create a linked list
exports.create_linked_list = [
  //validate the request
  body("title", "Title is required").notEmpty(),
  body("description", "Description is required").notEmpty(),
  body("learningObjects", "Learning Objects is required").notEmpty(),
  //process the request
  (req, res, next) => {
    console.log("create");
    // Extract the validation errors from a request.
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    //create a new linked list
    const linkedList = new LinkedList({
      title: req.body.title,
      description: req.body.description,
      learningObjects: req.body.learningObjects,
    });
    //save the linked list
    linkedList.save((err, data) => {
      if (err) {
        res.status(500).json({
          error: err,
        });
      }
      res.status(200).json({
        data,
      });
    });
  },
];

//update a linked list by id
exports.update_linked_list_by_id = [
  //validate the request
  body("title", "Title is required").notEmpty(),
  body("description", "Description is required").notEmpty(),
  body("learningObjects", "Learning Objects is required").notEmpty(),
  //process the request
  (req, res, next) => {
    // Extract the validation errors from a request.
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    //update the linked list
    LinkedList.findByIdAndUpdate(
      req.params.LinkedListId,
      {
        title: req.body.title,
        description: req.body.description,
        learningObjects: req.body.learningObjects,
      },
      (err, data) => {
        if (err) {
          res.status(500).json({
            error: err,
          });
        }
        res.status(200).json({
          data,
        });
      }
    );
  },
];

//delete a linked list by id
exports.delete_linked_list_by_id = [
  (req, res, next) => {
    LinkedList.findByIdAndDelete(
      req.params.LinkedListId,
      function (err, linkedList) {
        if (err) {
          res.status(500).json({
            error: err,
          });
        }
        res.status(200).json({
          data: linkedList,
        });
      }
    );
  },
];
