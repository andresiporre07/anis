var Resource = require("../models/resource.model");
const { body, validationResult } = require("express-validator");

exports.resource_get_all = [
  (req, res, next) => {
    Resource.find({}, (err, resources) => {
      if (err) {
        res.status(500).json({
          error: err,
        });
      }
      res.status(200).json(resources);
    });
  },
];

exports.Resource_get_by_id = [
  //get by id
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    Resource.findById(req.params.ResourceId, (err, data) => {
      if (err) {
        res.status(500).json({
          error: err,
        });
      }
      res.status(200).json({
        data,
      });
    });
  },
];

exports.Resource_create = [
  //validate the request
  body("title", "Title is required").notEmpty(),
  body("resource", "Resource is required").notEmpty(),
  body("type", "Type is required").notEmpty(),
  body("description", "Description is required").notEmpty(),

  //process the request
  (req, res, next) => {
    console.log("create");
    // Extract the validation errors from a request.
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    //create a new resource
    const resource = new Resource({
      title: req.body.title,
      resource: req.body.resource,
      type: req.body.type,
      description: req.body.description,
    });
    //save the resource
    resource.save((err, data) => {
      if (err) {
        return next(err);
      }
      res.status(201).json({ data });
    });
  },
];

//diagram update put
exports.Resource_update_by_id = [
  //validate the request
  body("title", "Title is required").notEmpty(),
  body("resource", "Resource is required").notEmpty(),
  body("type", "Type is required").notEmpty(),
  body("description", "Description is required").notEmpty(),
  //process the request
  (req, res, next) => {
    // Extract the validation errors from a request.
    const errors = validationResult(req);
    // Create a Resource object with escaped and trimmed data.
    Resource.findByIdAndUpdate(
      req.params.ResourceId,
      {
        title: req.body.title,
        resource: req.body.resource,
        type: req.body.type,
        description: req.body.description,
      },
      { new: true },
      (err, data) => {
        if (err) {
          res.status(500).json({
            error: err,
          });
        }
        res.status(200).json(data);
      }
    );
  },
];
exports.Resource_delete_by_id = [
  (req, res, next) => {
    Resource.findByIdAndDelete(req.params.ResourceId, function (err, resource) {
      if (err) {
        res.status(500).json({
          error: err,
        });
      }
      res.status(200).json({
        resource,
      });
    });
  },
];
