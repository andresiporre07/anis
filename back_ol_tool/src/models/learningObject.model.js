const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const learningObjectSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  abstract: {
    type: String,
    required: true,
  },
  knowledgeArea: {
    type: String,
    required: true,
  },
  relationKnowledgeArea: {
    type: String,
    required: false,
  },
  author: {
    type: String,
    required: true,
  },
  degree: {
    type: String,
    required: true,
  },
  criteriaInclution: {
    type: String,
    required: false,
  },
  learningModel: {
    type: String,
    required: true,
  },
  license: {
    type: String,
    required: true,
  },
  keywords: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("LearningObject", learningObjectSchema);















