const express = require("express");
const fileController = require("../controllers/file.controller");
const router = express.Router();

router.post("/", fileController.Resource_create_file);
router.put("/:fileId", fileController.Resource_update_file);
router.delete("/:fileId", fileController.Resource_delete_file);

module.exports = router;
