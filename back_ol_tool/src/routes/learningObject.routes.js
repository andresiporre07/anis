const express = require("express");
const learningObjectController = require("../controllers/learningObject.controller");
const router = express.Router();

router.get("/", learningObjectController.LearningObject_get_all);
router.get(
  "/:LearningObjectId",
  learningObjectController.LearningObject_get_by_id
);
router.post("/", learningObjectController.LearningObject_create);
router.put(
  "/:LearningObjectId",
  learningObjectController.LearningObject_update_by_id
);
router.delete(
  "/:LearningObjectId",
  learningObjectController.LearningObject_delete_by_id
);

module.exports = router;
