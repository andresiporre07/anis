const express = require("express");
const linkedListController = require("../controllers/linkedList.controller");
const router = express.Router();

router.post("/", linkedListController.create_linked_list);
router.get("/", linkedListController.get_all_linked_list);
router.get("/:LinkedListId", linkedListController.get_linked_list_by_id);
router.put("/:LinkedListId", linkedListController.update_linked_list_by_id);
router.delete("/:LinkedListId", linkedListController.delete_linked_list_by_id);

module.exports = router;
