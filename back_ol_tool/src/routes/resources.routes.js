const express = require("express");
const resourceController = require("../controllers/resource.controller");
const router = express.Router();

router.post("/", resourceController.Resource_create);
router.get("/", resourceController.resource_get_all);
router.get("/:ResourceId", resourceController.Resource_get_by_id);
router.put("/:ResourceId", resourceController.Resource_update_by_id);
router.delete("/:ResourceId", resourceController.Resource_delete_by_id);

module.exports = router;
