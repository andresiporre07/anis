import React from "react";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./pages/Home";
import TextEditor from "./pages/TextEditor";
import ImageEditor from "./pages/ImageEditor";
import AudioEditor from "./pages/AudioEditor";
import NavBar from "./components/NavBar";
import ResourcesPage from "./pages/Resources";
import LOpage from "./pages/LearningObjects";
import SlidePage from "./pages/LO/slides";
import RouteInterestPage from "./pages/LO/route-interest";
import NewLOPage from "./pages/LO/newLO";
// import EditAudio from './components/Audio/EditAudio'
import LayoutAudio from "./pages/LayoutAudio";
import Slide from "./pages/LO/Slide";

function App() {
  return (
    <BrowserRouter>
      <NavBar></NavBar>
      <Routes>
        <Route path="/" exact element={<HomePage />}></Route>
        <Route path="/resources" element={<ResourcesPage />}></Route>
        <Route
          path="/resources/text-editor/:idResource"
          element={<TextEditor />}
        ></Route>
        <Route
          path="/resources/image-editor/:idResource"
          element={<ImageEditor />}
        ></Route>
        <Route path="/resources/audio-editor/" element={<LayoutAudio />}>
          <Route path=":idResource" element={<AudioEditor />}></Route>
          {/* <Route path="edit/:idResource" element={<EditAudio/>}></Route> */}
        </Route>
        <Route path="/learningObjects" element={<LOpage />}></Route>
        <Route path="/learningObjects/slides" element={<SlidePage />}></Route>
        <Route
          path="/learningObjects/slides/:idSlide"
          element={<Slide />}
        ></Route>
        <Route
          path="/learningObjects/route-interest/"
          element={<RouteInterestPage />}
        ></Route>
        <Route path="/learningObjects/:idLO" element={<NewLOPage />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
