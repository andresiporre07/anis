import { Delete, Edit, ExpandMore } from "@mui/icons-material";
import VisibilityIcon from "@mui/icons-material/Visibility";
import {
  Accordion,
  AccordionActions,
  AccordionDetails,
  AccordionSummary,
  Alert,
  Dialog,
  Fab,
  FormControlLabel,
  Modal,
  Radio,
  Stack,
  Typography,
} from "@mui/material";
import {
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { getTypeLink } from "../utilities/Paths";
import Parser from "html-react-parser";
import { useRequest } from "../hooks/useResponse";
import useModal from "../hooks/useModal";
import * as AiIcons from "react-icons/ai";
import * as IoIcons from "react-icons/io";
import "./card.css";
import services from "../services/services";

const CardLO = (props) => {
  const navigate = useNavigate();
  const [
    loadingDel,
    errorDel,
    successDel,
    responseDel,
    statusDel,
    handleRequestDel,
  ] = useRequest({
    methodRequest: services.deleteLO,
  });
  const [isOpen, openModal, closeModal] = useModal(false);
  const [open, setOpen] = useState(false);

  return (
    <div>
      <Accordion
        sx={{
          color: "white",
        }}
      >
        <AccordionSummary
          expandIcon={<ExpandMore sx={{ color: "white" }} />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          sx={{ background: "#172B4D" }}
        >
          <div
            style={{
              padding: "10px",
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <section className="section">
              <div key={props.content._id}>
                <IoIcons.IoIosJournal
                  style={{
                    fontSize: "1.5rem",
                  }}
                />
              </div>
              <div>Titulo: {props.content.title}</div>
            </section>
          </div>
        </AccordionSummary>
        <AccordionDetails>
          <div>
            <div
              style={{
                background: "#EBECF0",
                color: "black",
                padding: "20px",
              }}
              key={props.content._id}
            >
              {props.content.abstract}
            </div>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "flex-end",
              background: "linear-gradient(180deg, #EBECF0 50%, #FAFBFC 50%)",
              color: "black",
            }}
          >
            <div
              style={{
                minWidth: "100px",
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end",
              }}
            ></div>
          </div>
        </AccordionDetails>
        <AccordionActions>
          <Stack direction="row" spacing={1}>
            <Fab
              size="small"
              color="info"
              onClick={() => navigate(`/learningObjects/${props.content._id}`)}
            >
              <Edit />
            </Fab>
            <Fab
              size="small"
              color="secondary"
              onClick={() => {
                handleRequestDel(props.content._id);
                openModal();
              }}
            >
              <Delete />
            </Fab>
          </Stack>
        </AccordionActions>
        <Dialog
          open={open}
          onClose={() => setOpen(false)}
          aria-labelledby="dialog-title"
          aria-describedby="dialog-description"
        >
          <DialogTitle id="dialog-title">{props.content.title}</DialogTitle>
          <DialogContent>
            {/* {props.content.type === "text" ? (
                      <DialogContentText id="dialog-description">
                      {Parser(props.content.resource)}
                      </DialogContentText>
                  ) : props.content.type === "image" ? (
                      <img
                      width="100%"
                      height="100%"
                      src={props.content.resource.preview.path}
                      />
                  ) : null} */}
          </DialogContent>
        </Dialog>
        <Dialog
          open={isOpen}
          onClose={() => {
            window.location.reload();
          }}
        >
          <Alert
            onClose={() => {
              window.location.reload();
            }}
          >
            Se elimino el recurso exitosamente!
          </Alert>
        </Dialog>
      </Accordion>
    </div>
  );
};

export default CardLO;
