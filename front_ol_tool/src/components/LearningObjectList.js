import React from "react";
import Card from "./Card";

const ListaLO = (props) => {
  if (props.list.length !== 0) {
    return (
      <div style={{ width: "90%" }}>
        {props.list.map((element) => {
          return (
            <div
              key={element._id}
              style={{
                marginTop: "20px",
                marginBottom: "20px",
              }}
            >
              <Card content={element} />
            </div>
          );
        })}
      </div>
    );
  } else {
    return <div>Aun no tiene ningun slide de objetos de aprendizaje</div>;
  }
};
export default ListaLO;
