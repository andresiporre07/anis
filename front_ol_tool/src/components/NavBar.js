import React,{useState} from 'react'
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import {Link} from 'react-router-dom';
import { SidebarData, ResourcesOption, LO_Option} from "./SideBarData";
import './Navbar.css';
import {IconContext} from 'react-icons';
import { Accordion, AccordionDetails, AccordionSummary } from '@mui/material';
import { ExpandMore } from '@mui/icons-material';

function NavBar() {
    const [sidebar,setSidebar] = useState(false)
    const showSideBar = () => setSidebar(!sidebar)

    return (
      <>
        <IconContext.Provider value={{ color: "#fff" }}>
          <div
            className="navbar"
            style={{
              zIndex: "999",
            }}
          >
            <Link to="#" className="menu-bars">
              <FaIcons.FaBars onClick={showSideBar} />
            </Link>
          </div>
          <nav className={sidebar ? "nav-menu active" : "nav-menu"}>
            <ul className="nav-menu-items">
              <li className="navbar-toggle">
                <Link to="#" className="menu-bars" onClick={showSideBar}>
                  <AiIcons.AiOutlineClose />
                </Link>
              </li>
              <li key={SidebarData[0]} className={SidebarData[0].cName}>
                <Link to={SidebarData[0].path}>
                  {SidebarData[0].icon}
                  <span>{SidebarData[0].title}</span>
                </Link>
              </li>
              
              <Accordion
                sx={{
                  background: "#060b26",
                }}
              >
                <AccordionSummary
                  expandIcon={<ExpandMore sx={{ color: "white" }} />}
                >
                  <li key={SidebarData[1]} className={SidebarData[1].cName}>
                    <Link to={SidebarData[1].path}>
                      {SidebarData[1].icon}
                      <span>{SidebarData[1].title}</span>
                    </Link>
                  </li>
                </AccordionSummary>
                <AccordionDetails>
                  {ResourcesOption.map((item, index) => {
                    return (
                      <li key={index} className={item.cName} onClick={showSideBar}>
                        <Link to={item.path}>
                          {item.icon}
                          <span>{item.title}</span>
                        </Link>
                      </li>
                    );
                  })}
                </AccordionDetails>
              </Accordion>

              <Accordion
                sx={{
                  background: "#060b26",
                }}
              >
                <AccordionSummary
                  expandIcon={<ExpandMore sx={{ color: "white" }} />}
                >
                  <li key={SidebarData[2]} className={SidebarData[2].cName}>
                    <Link to={SidebarData[2].path}>
                      {SidebarData[2].icon}
                      <span>{SidebarData[2].title}</span>
                    </Link>
                  </li>
                </AccordionSummary>
                <AccordionDetails>
                  {LO_Option.map((item, index) => {
                    return (
                      <li key={index} className={item.cName} onClick={showSideBar}>
                        <Link to={item.path}>
                          {item.icon}
                          <span>{item.title}</span>
                        </Link>
                      </li>
                    );
                  })}
                </AccordionDetails>
              </Accordion>
            </ul>
          </nav>
        </IconContext.Provider>
      </>
    );
}

export default NavBar