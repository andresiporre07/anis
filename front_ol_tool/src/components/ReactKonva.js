import React, { useState, useRef, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
// import "./HomePage.css";
import { Stage, Layer } from "react-konva";
import Rectangle from "../components/Konva/Rectangle";
import Circle from "../components/Konva/Circle";
import { addTextNode } from "../components/Konva/textNode";
import Image from "../components/Konva/Image";
// const uuidv1 = require("uuid/v1");
import { v1 as uuidv1 } from "uuid";
import { Card } from "@mui/material";
import services from "../services/services";

export default function ReactKonva(props) {
  const [resource, setResource] = useState({
    rectangles: [],
    circles: [],
    images: [],
    selectedId: null,
    shapes: [],
    preview: null,
  });
  const [selectedId, selectShape] = useState(null);
  const [shapes, setShapes] = useState([]);
  const [, updateState] = React.useState();
  const stageEl = React.createRef();
  const layerEl = React.createRef();
  const fileUploadEl = React.createRef();
  const getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
  };
  const addRectangle = () => {
    const rect = {
      x: getRandomInt(100),
      y: getRandomInt(100),
      width: 50,
      height: 50,
      fill: "red",
      id: `rect${resource.rectangles.length + 1}`,
    };
    const rects = resource.rectangles.concat([rect]);
    // setRectangles(rects);
    setResource({
      ...resource,
      rectangles: rects,
    });
    const shs = shapes.concat([`rect${resource.rectangles.length + 1}`]);
    setShapes(shs);
  };
  const addCircle = () => {
    const circ = {
      x: getRandomInt(100),
      y: getRandomInt(100),
      width: 50,
      height: 50,
      fill: "red",
      id: `circ${resource.circles.length + 1}`,
    };
    const circs = resource.circles.concat([circ]);
    // setCircles(circs);
    setResource({
      ...resource,
      circles: circs,
    });
    const shs = shapes.concat([`circ${resource.circles.length + 1}`]);
    setShapes(shs);
  };
  const drawText = () => {
    const id = addTextNode(stageEl.current.getStage(), layerEl.current);
    const shs = shapes.concat([id]);
    setShapes(shs);
  };
  const drawImage = () => {
    fileUploadEl.current.click();
  };
  const forceUpdate = React.useCallback(() => updateState({}), []);
  const fileChange = (ev) => {
    let file = ev.target.files[0];
    let reader = new FileReader();
    reader.addEventListener(
      "load",
      () => {
        const id = uuidv1();
        resource.images.push({
          content: reader.result,
          position: {
            x: 50,
            y: 50,
            width: 100,
            height: 100,
          },
          id,
        });
        // setImages(images);
        setResource({
          ...resource,
          images: resource.images,
        });
        fileUploadEl.current.value = null;
        shapes.push(id);
        setShapes(shapes);
        forceUpdate();
      },
      false
    );
    if (file) {
      reader.readAsDataURL(file);
    }
  };
  document.addEventListener("keydown", (ev) => {
    if (ev.code == "Delete") {
      let index = resource.circles.findIndex((c) => c.id === selectedId);
      if (index != -1) {
        resource.circles.splice(index, 1);
        // setCircles(circles);
        setResource({
          ...resource,
          circles: resource.circles,
        });
      }
      index = resource.rectangles.findIndex((r) => r.id === selectedId);
      if (index != -1) {
        resource.rectangles.splice(index, 1);
        // setRectangles(rectangles);
        setResource({
          ...resource,
          rectangles: resource.rectangles,
        });
      }
      index = resource.images.findIndex((r) => r.id === selectedId);
      if (index != -1) {
        resource.images.splice(index, 1);
        // setImages(images);
        setResource({
          ...resource,
          images: resource.images,
        });
      }
      forceUpdate();
    }
  });

  const fetchImages = async () => {
    if (props.value.resource.images) {
      const promises = [];
      props.value.resource.images.map((item) => {
        const responseF = services.getFile(item.content.path);
        promises.push(responseF);
      });
      const responses = await Promise.all(promises);

      setResource({
        ...resource,
        ...props.value.resource,
        images: props.value.resource.images.map((item, index) => {
          return {
            ...item,
            oldContent: {
              ...item.content,
            },
            content: `data:image/png;base64,${responses[index]}`,
          };
        }),
      });
    } else {
      setResource({
        ...resource,
        ...props.value.resource,
      });
    }
    forceUpdate();
  };
  // const exportPreview = () => {
  //   const uri = stageEl.current.toDataURL();
  //   console.log(uri);
  //   return uri;
  // };
  useEffect(() => {
    if (props.value) {
      fetchImages();
    }
  }, []);
  useEffect(() => {
    props.save(resource);
  }, [resource]);

  const replaceAttr = (images, position, id) => {};

  return (
    <Card>
      <ButtonGroup>
        <Button variant="secondary" onClick={addRectangle}>
          Rectangulo
        </Button>
        <Button variant="secondary" onClick={addCircle}>
          Circulo
        </Button>
        <Button variant="secondary" onClick={drawText}>
          Texto
        </Button>
        <Button variant="secondary" onClick={drawImage}>
          Imagen
        </Button>
      </ButtonGroup>
      <input
        style={{ display: "none" }}
        type="file"
        ref={fileUploadEl}
        onChange={fileChange}
      />
      <Stage
        style={{
          borderTop: "1px solid #ccc",
        }}
        // width={window.innerWidth * 0.9}
        // height={window.innerHeight * 0.8}
        width={700}
        height={600}
        ref={stageEl}
        onMouseDown={(e) => {
          // deselect when clicked on empty area
          const clickedOnEmpty = e.target === e.target.getStage();
          if (clickedOnEmpty) {
            selectShape(null);
          }
        }}
      >
        <Layer
          ref={layerEl}
          style={{
            width: "100%",
            height: "100%",
          }}
        >
          {resource.rectangles.map((rect, i) => {
            return (
              <Rectangle
                key={i}
                shapeProps={rect}
                isSelected={rect.id === selectedId}
                onSelect={() => {
                  selectShape(rect.id);
                }}
                onChange={(newAttrs) => {
                  const rects = resource.rectangles.slice();

                  rects[i] = newAttrs;
                  // setRectangles(rects);
                  setResource({
                    ...resource,
                    rectangles: rects,
                  });
                }}
              />
            );
          })}
          {resource.circles.map((circle, i) => {
            return (
              <Circle
                key={i}
                shapeProps={circle}
                isSelected={circle.id === selectedId}
                onSelect={() => {
                  selectShape(circle.id);
                }}
                onChange={(newAttrs) => {
                  const circs = resource.circles.slice();
                  circs[i] = newAttrs;
                  // setCircles(circs);
                  setResource({
                    ...resource,
                    circles: circs,
                  });
                }}
              />
            );
          })}
          {resource.images.map((image, i) => {
            return (
              <Image
                key={i}
                imageUrl={image.content}
                isSelected={image.id === selectedId}
                onSelect={() => {
                  selectShape(image.id);
                }}
                onChange={(newAttrs) => {
                  const imgs = resource.images.slice();
                  imgs[i] = newAttrs;

                  const newImages = [...resource.images];
                  newImages[i] = {
                    ...newImages[i],
                    position: {
                      x: newAttrs.x,
                      y: newAttrs.y,
                      width: newAttrs.width,
                      height: newAttrs.height,
                    },
                  };
                  setResource({
                    ...resource,
                    images: newImages,
                  });
                }}
              />
            );
          })}
        </Layer>
      </Stage>
    </Card>
  );
}
