import React from "react";
import * as AiIcons from "react-icons/ai";
import * as IoIcons from "react-icons/io";

export const SidebarData = [
  {
    title: "Inicio",
    path: "/",
    icon: (
      <AiIcons.AiFillHome
        style={{
          fontSize: "1.35rem",
        }}
      />
    ),
    cName: "nav-text",
  },
  {
    title: "Recursos",
    path: "resources",
    icon: (
      <IoIcons.IoIosApps
        style={{
          fontSize: "2rem",
        }}
      />
    ),
    cName: "nav-text",
  },
  {
    title: "Objetos de Aprendizaje",
    path: "learningObjects",
    icon: (
      <IoIcons.IoIosBookmarks
        style={{
          fontSize: "1.8rem",
        }}
      />
    ),
    cName: "nav-text",
  },
];

export const ResourcesOption = [
  {
    title: "Texto",
    path: "resources/text-editor/new",
    icon: <IoIcons.IoIosPaper />,
    cName: "nav-text",
  },
  {
    title: "Imagen",
    path: "resources/image-editor/new",
    icon: <IoIcons.IoIosImage />,
    cName: "nav-text",
  },
  {
    title: "Audio",
    path: "resources/audio-editor/new",
    icon: <IoIcons.IoIosMusicalNotes />,
    cName: "nav-text",
  },
];

export const LO_Option = [
  {
    title: "Slides",
    path: "learningObjects/slides",
    icon: <IoIcons.IoIosPaper />,
    cName: "nav-text",
  },
  {
    title: "Ruta de Intereses",
    path: "learningObjects/route-interest",
    icon: <IoIcons.IoIosImage />,
    cName: "nav-text",
  },
];
