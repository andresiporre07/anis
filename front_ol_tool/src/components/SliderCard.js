import { Delete, Edit, ExpandMore } from "@mui/icons-material";
import VisibilityIcon from "@mui/icons-material/Visibility";
import {
  Accordion,
  AccordionActions,
  AccordionDetails,
  AccordionSummary,
  Alert,
  Dialog,
  Fab,
  Modal,
  Stack,
  Typography,
} from "@mui/material";
import {
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from "@mui/material";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { getTypeLink } from "../utilities/Paths";
import Parser from "html-react-parser";
import services from "../services/services";
import { useRequest } from "../hooks/useResponse";
import useModal from "../hooks/useModal";
import * as AiIcons from "react-icons/ai";
import * as IoIcons from "react-icons/io";
import "./card.css";

const SliderCard = (props) => {
  const [
    loadingDel,
    errorDel,
    successDel,
    responseDel,
    statusDel,
    handleRequestDel,
  ] = useRequest({
    methodRequest: services.deleteResource,
  });
  const [isOpen, openModal, closeModal] = useModal(false);

  return (
    <Accordion
      sx={{
        color: "white",
      }}
    >
      <AccordionSummary
        expandIcon={<ExpandMore sx={{ color: "white" }} />}
        aria-controls="panel1a-content"
        id="panel1a-header"
        sx={{ background: "#172B4D" }}
      ></AccordionSummary>
      <AccordionDetails></AccordionDetails>
      <AccordionActions>
        <Stack direction="row" spacing={1}>
          <Link
            to={`/resources${getTypeLink(props.content.type)}/${
              props.content._id
            }`}
          >
            <Fab size="small" color="primary">
              <Edit />
            </Fab>
          </Link>
          <Fab
            size="small"
            color="secondary"
            onClick={() => {
              handleRequestDel(props.content._id);
              openModal();
            }}
          >
            <Delete />
          </Fab>
        </Stack>
      </AccordionActions>

      <Dialog
        open={isOpen}
        onClose={() => {
          window.location.reload();
        }}
      >
        <Alert
          onClose={() => {
            window.location.reload();
          }}
        >
          Se elimino el slide exitosamente!
        </Alert>
      </Dialog>
    </Accordion>
  );
};
export default SliderCard;
