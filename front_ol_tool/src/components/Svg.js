import React from "react";

const Svg = (props) => {

  const [position, setPosition] = React.useState({
    x: 100,
    y: 100,
    active: false,
    offset: { }
  });
  const handlePointerDown = e => {
    const el = e.target;
    const bbox = e.target.getBoundingClientRect();
    const x = e.clientX - bbox.left;
    const y = e.clientY - bbox.top;
    el.setPointerCapture(e.pointerId);
    setPosition({
      ...position,
      active: true,
      offset: {
        x,
        y
      }
    });
  };

  const handlePointerMove = e => {
    const bbox = e.target.getBoundingClientRect();
    const x = e.clientX - bbox.left;
    const y = e.clientY - bbox.top;
    if (position.active) {
      setPosition({
        ...position,
        x: position.x - (position.offset.x - x),
        y: position.y - (position.offset.y - y)
      });
    }
  };

  const handlePointerUp = e => {
    setPosition({
      ...position,
      active: false
    });
  };

  const paintCircle = () =>{
    alert('se dibuja circulo')
  }

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        border: "1px solid black",
        minWidth: "500px",
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          width: "100%",
          flexDirection: "row",
          borderBottom: "1px solid black",
        }}
      >
        <button>subir imagen</button>
        <button onClick={paintCircle}>Agregar Circulo</button>
        <button>Agregar linea</button>
        <button>Descargar</button>
      </div>
      <svg viewBox="0 0 500 500" width="500" height="450">
        {/* {resource.resource} */}
        <circle
          cx={position.x}
          cy={position.y}
          r={30}
          onPointerDown={handlePointerDown}
          onPointerUp={handlePointerUp}
          onPointerMove={handlePointerMove}
          fill={position.active ? "blue" : "black"}   
        />
      </svg>
    </div>
  );
};

export default Svg;
