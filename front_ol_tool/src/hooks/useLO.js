import React, { useState, useEffect } from "react";
import services from "../services/services";
import { TYPE } from "../utilities/Constant";
const useLO = ({ idLO, type }) => {
  const [learningObject, setLearningObject] = useState({
    _id: idLO,
    title: "",
    abstract: "",
    knowledgeArea: "",
    relationKnowledgeArea: "",
    author: "",
    degree: "",
    criteriaInclution: "",
    learningModel: "",
    license: "",
    keywords: "",
    content: [],
  });
  const [loading, setLoading] = useState(false);

  const fetchList = async () => {
    setLoading(true);
    if (idLO !== "new") {
      const response = await services.getLOByID(idLO);
      setLearningObject({
        ...response.data,
        content: response.data
          ? response.data.content
          : JSON.parse(response.data.content),
      });
    } else {
      setLearningObject({
        title: "",
        abstract: "",
        knowledgeArea: "",
        relationKnowledgeArea: "",
        author: "",
        degree: "",
        criteriaInclution: "",
        learningModel: "",
        license: "",
        keywords: "",
        content: [],
      });
    }
    setLoading(false);
  };

  useEffect(() => {
    fetchList();
  }, []);
    

  const isEnabled = () => {
    return learningObject.title !== "" && learningObject.description !== "";
  };
  const newlearningObject = async () => {
    if (isEnabled()) {
      setLoading(true);
      if (idLO === "new") {
          const response = await services.postLO(learningObject);
      } else {
        const response = await services.putLO(idLO, learningObject);
      }
      setLoading(false);
    }
  };

  return [
    learningObject,
    loading,
    handlelearningObject,
    handleTitle,
    handleDescription,
    newlearningObject,
    isEnabled,
  ];
};

export default useLO;
