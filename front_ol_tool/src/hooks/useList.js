import React, { useState, useEffect } from "react";
import services from "../services/services";

const useList = ({ methodRequest }) => {
  const [list, setList] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchList = async () => {
    setLoading(true);
    const response = await methodRequest();
    setList(response);
    setLoading(false);
  };

  useEffect(() => {
    fetchList();
  }, []);

  return [list, loading];
};

export default useList;
