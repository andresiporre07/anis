import React, { useState, useEffect } from "react";
import services from "../services/services";
import { TYPE } from "../utilities/Constant";
const useResource = ({ idR, type }) => {
  const [resource, setResource] = useState({
    _id: idR,
    title: "",
    description: "",
    resource: "",
    type: type,
  });
  const [loading, setLoading] = useState(false);

  const fetchList = async () => {
    setLoading(true);
    if (idR !== "new") {
      const response = await services.getResourceById(idR);
      setResource({
        _id: idR,
        title: response.data.title,
        description: response.data.description,
        resource:
          response.data.type === TYPE.TEXT
            ? response.data.resource
            : JSON.parse(response.data.resource),
        type: response.data.type,
      });
    } else {
      setResource({
        title: "",
        description: "",
        resource: "",
        type: type,
      });
    }
    setLoading(false);
  };

  useEffect(() => {
    fetchList();
  }, []);

  const handleResource = (e) => {
    setResource({
      ...resource,
      resource: e,
    });
  };
  const handleTitle = (e) => {
    setResource({
      ...resource,
      title: e.target.value,
    });
  };
  const handleDescription = (e) => {
    setResource({
      ...resource,
      description: e.target.value,
    });
  };

  const isEnabled = () => {
    return (
      resource.title !== "" &&
      resource.resource !== "" &&
      resource.description !== ""
    );
  };
  const newResource = async (referencia) => {
    if (isEnabled()) {
      setLoading(true);
      if (idR === "new") {
        if (resource.type === TYPE.IMAGE) {
          const newResource = [];
          const promises = [];
          resource.resource.images.map((item) => {
            const responseF = services.saveFile(item.content);
            promises.push(responseF);
          });
          const responseProm = await Promise.all(promises);
          resource.resource.images.map((item, index) => {
            newResource.push({
              content: responseProm[index].file,
              id: item.id,
              position: item.position,
            });
          });

          const filePreview = await services.saveFile(referencia);

          const response = await services.postResource(
            resource.type === TYPE.TEXT
              ? resource
              : {
                  ...resource,
                  resource: JSON.stringify({
                    ...resource.resource,
                    images: newResource,
                    preview: filePreview.file,
                  }),
                }
          );
          setResource({
            _id: response.data._id,
            title: response.data.title,
            description: response.data.desciption,
            resource: response.data.resource,
            type: response.data.type,
          });
        } else {
          const response = await services.postResource(
            resource.type === TYPE.TEXT
              ? resource
              : {
                  ...resource,
                  resource: JSON.stringify(resource.resource),
                }
          );
          setResource({
            _id: response.data._id,
            title: response.data.title,
            description: response.data.desciption,
            resource: response.data.resource,
            type: response.data.type,
          });
        }
      } else {
        if (resource.type === TYPE.IMAGE) {
          const newResource = [];
          const promises = [];
          const filePreview = await services.saveFile(referencia);
          resource.resource.images.map((item) => {
            if (!item.oldContent) {
              const responseF = services.saveFile(item.content);
              promises.push(responseF);
            } else {
              promises.push(
                Promise.resolve({
                  file: {
                    ...item.oldContent,
                  },
                })
              );
            }
          });
          const responseProm = await Promise.all(promises);

          resource.resource.images.map((item, index) => {
            newResource.push({
              content: responseProm[index].file,
              id: item.id,
              position: item.position,
            });
          });
          const response = await services.putResource(
            resource.type === TYPE.TEXT
              ? resource
              : {
                  ...resource,
                  resource: JSON.stringify({
                    ...resource.resource,
                    images: newResource,
                    preview: filePreview.file,
                  }),
                },
            idR
          );
          setResource({
            _id: idR,
            title: response.title,
            description: response.desciption,
            resource: response.resource,
            type: response.type,
          });
        } else {
          const response = await services.putResource(
            resource.type === TYPE.TEXT
              ? resource
              : {
                  ...resource,
                  resource: JSON.stringify(resource.resource),
                },
            idR
          );
          setResource({
            _id: idR,
            title: response.title,
            description: response.desciption,
            resource: response.resource,
            type: response.type,
          });
        }
      }
      setLoading(false);
    }
  };

  return [
    resource,
    loading,
    handleResource,
    handleTitle,
    handleDescription,
    newResource,
    isEnabled,
  ];
};

export default useResource;
