import React, { useState } from "react";

export const useRequest = ({ methodRequest }) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [response, setResponse] = useState("");
  const [status, setStatus] = useState("");
  const handleRequest = async (request) => {
    setLoading(true);
    setError("");
    setSuccess("");
    try {
      const response = await methodRequest(request);
      const data = response;
      setLoading(false);
      setResponse(data.data);
    } catch (error) {
      setLoading(false);
      setError(error.message);
    }
  };

  return [loading, error, success, response, status, handleRequest];
};
