import React, { useState, useEffect } from "react";
import services from "../services/services";
import { TYPE } from "../utilities/Constant";
const useSlide = ({ idR }) => {
  const [slide, setSlide] = useState({
    _id: idR,
    title: "",
    description: "",
    learningObjects: [],
  });
  const [loading, setLoading] = useState(false);

  const fetchList = async () => {
    setLoading(true);
    if (idR !== "new" && idR !== undefined && idR !== null) {
      const response = await services.getSlideById(idR);
      setSlide({
        _id: response.data._id,
        title: response.data.title,
        description: response.data.description,
        learningObjects: JSON.parse(response.data.learningObjects),
      });
    } else {
      setSlide({
        title: "",
        description: "",
        learningObjects: [],
      });
    }
    setLoading(false);
  };

  useEffect(() => {
    fetchList();
  }, []);

  const handleSlide = (e) => {
    setSlide({
      ...slide,
      learningObjects: e,
    });
  };
  const handleTitle = (e) => {
    setSlide({
      ...slide,
      title: e.target.value,
    });
  };
  const handleDescription = (e) => {
    setSlide({
      ...slide,
      description: e.target.value,
    });
  };
  const isEnabled = () => {
    return slide.title.length !== "" && slide.description !== "";
  };

  const saveSlide = async () => {
    setLoading(true);

    if (idR === "new") {
      await services.postSlide({
        title: slide.title,
        description: slide.description,
        learningObjects: JSON.stringify(slide.learningObjects),
      });
    } else {
      await services.putSlide(
        {
          _id: slide._id,
          title: slide.title,
          description: slide.description,
          learningObjects: JSON.stringify(slide.learningObjects),
        },
        idR
      );
      alert("Slide actualizado!!");
    }

    setLoading(false);
  };

  const deleteSlide = async () => {
    setLoading(true);
    await services.deleteSlide(idR);
    setLoading(false);
  };

  return [
    slide,
    loading,
    handleSlide,
    handleTitle,
    handleDescription,
    saveSlide,
    isEnabled,
    setSlide,
    deleteSlide,
  ];
};

export default useSlide;
