import React from 'react'

function HomePage(){
    return(
        // <div>holla</div>
        <div style={{background: "#FAFBFC",
                        flex: "1",
                        width: "100%",
                        height: "100%",
                        minHeight: "95vh",}}>
            <h1 style={{ color: "#172B4D", 
                        textAlign: "center", 
                        fontFamily: "roboto" }} >Bienvenido!!</h1> 
        </div>
    );
}

export default HomePage;