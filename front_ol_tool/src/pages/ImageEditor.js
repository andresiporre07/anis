import React, { useState, useRef } from "react";
import { Save } from "@mui/icons-material";
import {
  Alert,
  AlertTitle,
  Box,
  Button,
  CircularProgress,
  Dialog,
  Stack,
} from "@mui/material";
import useResource from "../hooks/useResource";
import useModal from "../hooks/useModal";
import { Link, useParams } from "react-router-dom";
import { TYPE, MODULE } from "../utilities/Constant";
import ReactKonva from "../components/ReactKonva";
import "../components/card.css";

export default function ImageEditor() {
  let { idResource } = useParams();
  const [
    resource,
    loading,
    handleResource,
    handleTitle,
    handleDescription,
    newResource,
    isEnabled,
  ] = useResource({
    idR: idResource,
    type: TYPE.IMAGE,
  });
  const [isOpenModal, openModal, closeModal] = useModal(false);

  return (
    <div>
      {loading ? (
        <CircularProgress />
      ) : (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            paddingTop: "40px",
            flexDirection: "column",
          }}
        >
          <h2>Editor de Imagen</h2>
          <br></br>

          <section className="section2">
            <div>
              <label>Titulo: </label>
            </div>
            <div>
              <input
                style={{ alignSelf: "flex-start", marginBottom: "20px" }}
                type="text"
                placeholder="Titulo"
                value={resource.title}
                onChange={handleTitle}
              ></input>
            </div>

            <div>
              <label>Descripcion: </label>
            </div>
            <div>
              <textarea
                style={{
                  alignSelf: "flex-start",
                  marginBottom: "20px",
                  width: "290px",
                  height: "100px",
                  resize: "none",
                }}
                // type="text"
                placeholder="Descripcion"
                value={resource.description}
                onChange={handleDescription}
              ></textarea>
            </div>
          </section>
          <div
            style={{
              maxWidth: "800px",
              margin: "2rem auto",
            }}
          >
            <ReactKonva save={handleResource} value={resource} />
          </div>

          <Stack
            spacing={2}
            direction="row"
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Link
              to="/resources"
              style={{
                textDecoration: "none",
              }}
            >
              <Button variant="outlined" color="error" onClick={(e) => {}}>
                Cancelar
              </Button>
            </Link>
            <Button
              variant="contained"
              color="primary"
              onClick={async (e) => {
                e.preventDefault();
                const resourceRe = document.getElementsByTagName("canvas")[1]
                  ? document.getElementsByTagName("canvas")[1]
                  : document.getElementsByTagName("canvas")[0];
                newResource(resourceRe.toDataURL());
                openModal();
              }}
              startIcon={<Save />}
            >
              Guardar
            </Button>
          </Stack>

          <Dialog
            open={isOpenModal}
            onClose={() => {
              closeModal();
              window.location.href = `/resources/image-editor/${resource._id}`;
            }}
          >
            {loading ? (
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  height: "100%",
                }}
              >
                <CircularProgress color="inherit" />
              </Box>
            ) : !isEnabled() ? (
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  flexDirection: "column",
                }}
              >
                <Alert severity="error">
                  <AlertTitle>Error</AlertTitle>
                  El <strong>titulo</strong> y el recurso son obligatorios
                  <Button onClick={closeModal} autoFocus>
                    Continuar
                  </Button>
                </Alert>
              </Box>
            ) : (
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  flexDirection: "column",
                }}
              >
                <Alert severity="success">
                  <AlertTitle>Exito</AlertTitle>
                  Su recurso se ha guardado con éxito!!
                  <Stack
                    sx={{
                      paddingTop: "1rem",
                    }}
                    direction="row"
                    spacing={2}
                  >
                    <Link to="/resources" style={{ textDecoration: "none" }}>
                      <Button>Salir</Button>
                    </Link>
                    <Link
                      to={`/resources/image-editor/${resource._id}`}
                      style={{ textDecoration: "none" }}
                    >
                      <Button
                        variant="contained"
                        color="success"
                        onClick={() => {
                          closeModal();
                          window.location.href = `/resources/image-editor/${resource._id}`;
                        }}
                      >
                        Continuar
                      </Button>
                    </Link>
                  </Stack>
                </Alert>
              </Box>
            )}
          </Dialog>
        </div>
      )}
    </div>
  );
}
