import React, { useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { Add } from "@mui/icons-material";
import {
  Accordion,
  Alert,
  Button,
  CircularProgress,
  Dialog,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from "@mui/material";
import useSlide from "../../hooks/useSlide";
import services from "../../services/services";
import Parser from "html-react-parser";
import * as AiIcons from "react-icons/ai";
import * as IoIcons from "react-icons/io";
import Carousel from "react-bootstrap/Carousel";
import useList from "../../hooks/useList";
import useModal from "../../hooks/useModal";
import ListaLO from "../../components/LearningObjectList";
import CardLO from "../../components/CardLO";
import { Row } from "react-bootstrap";
function Slide() {
  let { idSlide } = useParams();
  const [
    slide,
    loading,
    handleSlide,
    handleTitle,
    handleDescription,
    saveSlide,
    isEnabled,
    setSlide,
    deleteSlide,
  ] = useSlide({ idR: idSlide });
  const [listLO, setListLO] = useList({
    methodRequest: services.getLO,
  });
  const [radioB, setRadioB] = React.useState("female");
  const navigate = useNavigate();

  const [isOpenAdd, openAdd, closeAdd] = useModal();
  const [isOpenListLO, openListLO, closeListLO] = useModal();
  const [idOpenDel, openDel, closeDel] = useModal();
  const [idOpenSave, openSave, closeSave] = useModal();
  const handleChangeRadio = (event) => {
    setRadioB(event.target.value);
  };
  const [index, setIndex] = useState(0);
  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };
  const handleSave = () => {
    saveSlide();
    openSave();
    // window.location.reload();
  }
  const handleReturn = () => {
    navigate("/learningObjects/slides");
  }
  return (
    <div>
      {loading ? (
        <CircularProgress />
      ) : (
        <div
          style={{
            flex: "1",
            display: "flex",
            flexDirection: "row",
            width: "100%",
            height: "100%",
            minHeight: "90vh",
            background: "#D9FFBB",
          }}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "flex-start",
              minWidth: "300px",
              width: "25%",
              height: "100%",
              minHeight: "90vh",
              background: "#D9FFBB",
            }}
          >
            {/* botones */}
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                width: "100%",
                height: "14%",
                background: "#FFFFFF",
                justifyContent: "space-between",
                padding: "6px",
              }}
            >
              <Button
                variant="contained"
                color="success"
                disabled={!isEnabled()}
                //onClick={saveSlide}
                onClick={handleSave}
                sx={{
                  marginBottom: "6px",
                }}
              >
                Guardar
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={openAdd}
                sx={{
                  marginBottom: "6px",
                }}
              >
                Agregar
              </Button>
              <Button
                variant="contained"
                color="error"
                onClick={() => {
                  deleteSlide(slide._id);
                  openDel();
                }}
                sx={{
                  marginBottom: "6px",
                }}
              >
                Eliminar
              </Button>
            </div>
            {/* fields */}
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                width: "100%",
                height: "26%",
                // height: "200px",
                background: "#FFFFFF",
                justifyContent: "flex-start",
                padding: "12px",
              }}
            >
              <TextField
                id="outlined-required"
                label="Titulo"
                variant="outlined"
                value={slide.title}
                onChange={handleTitle}
                defaultValue=""
                sx={{
                  width: "80%",
                  height: "100%",
                  marginBottom: "6px",
                }}
              />
              <TextField
                id="outlined-required"
                label="Descripcion"
                variant="outlined"
                value={slide.description}
                onChange={handleDescription}
                defaultValue=""
                multiline
                rows="3"
                sx={{
                  width: "90%",
                  height: "100%",
                }}
              />
            </div>
            {/* carousel - lista de los LO */}
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                width: "100%",
                height: "60%",
                justifyContent: "flex-start",
                paddingTop: "3rem",
              }}
            >
              <h3 style={{ textAlign: "center"}}>LO Agregados</h3>
              <Carousel
                variant="dark"
                interval={null}
                activeIndex={index}
                onSelect={handleSelect}
              >
                {slide.learningObjects.map((element) => {
                  return (
                    <Carousel.Item style={{ height: "300px", width: "400px" }}>
                      {/* <img
                      className="d-block w-100"
                      src="https://media.geeksforgeeks.org/wp-content/uploads/20210425122716/1-300x115.png"
                      alt="First slide"
                    /> */}
                      <Carousel.Caption style={{ background: "#9ada81" }}>
                        <h4>{element.title}</h4>
                        <ul style={{ listStyleType: "none", padding: "0" }}>
                          <li style={{ textAlign: "left", padding: "0" }}>
                            <b>
                              <span>Resumen: </span>
                            </b>
                            {element.abstract}
                          </li>
                          {/* <li style={{textAlign:"left"}}><b><span>Area de Conocimiento: </span></b>{element.knowledgeArea}</li> */}
                          <li style={{ textAlign: "left" }}>
                            <b>
                              <span>Autor: </span>
                            </b>
                            {element.author}
                          </li>
                          <li style={{ textAlign: "left" }}>
                            <b>
                              <span>Nivel: </span>
                            </b>
                            {element.degree}
                          </li>
                          <li style={{ textAlign: "left" }}>
                            <b>
                              <span>Modelo Aprendizaje: </span>
                            </b>
                            {element.learningModel}
                          </li>
                          <li style={{ textAlign: "left" }}>
                            <b>
                              <span>Palabras Claves: </span>
                            </b>
                            {element.keywords}
                          </li>
                        </ul>
                      </Carousel.Caption>
                    </Carousel.Item>
                  );
                })}
              </Carousel>
            </div>
          </div>

          <div
            style={{
              display: "flex",
              flexDirection: "column",
              width: "75%",
              height: "100%",
              minHeight: "90vh",
              padding: "12px",
              // background: "#D9D9D9",
              background: "#E7E9EC",
              justifyContent: "flex-start",
              alignItems: "center",
            }}
          > 
            {slide.learningObjects.length > 0 &&
            index > -1 &&
            index < slide.learningObjects.length ? (
              <div>
                <Typography variant="h4" gutterBottom>
                  {slide.learningObjects[index].title}
                </Typography>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    width: "100%",
                    justifyContent: "space-between",
                  }}
                >
                  <Button
                    variant="outlined"
                    color="error"
                    onClick={() => {
                      setSlide({
                        ...slide,
                        learningObjects: [
                          ...slide.learningObjects.slice(0, index),
                          ...slide.learningObjects.slice(index + 1),
                        ],
                      });
                      setIndex(index - 1 < 0 ? 0 : index - 1);
                    }}
                  >
                    Eliminar LO
                  </Button>
                  <Button
                    variant="outlined"
                    color="primary"
                    onClick={() => {
                      navigate(
                        `/learningObjects/${slide.learningObjects[index]._id}`
                      );
                    }}
                  >
                    Editar LO
                  </Button>
                </div>
                <div
                  style={{
                    background: "#FFF",
                    padding: "20px",
                    minHeight: "700px",
                    minWidth: "800px",
                  }}
                >
                  {slide.learningObjects[index].content.map((resource) => (
                    <Row className="mb-3">
                      <div
                        sm={6}
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <div>
                          {resource.type === "text" ? (
                            <p>{Parser(resource.resource)}</p>
                          ) : resource.type === "image" ? (
                            <img
                              width="200px"
                              height="200px"
                              src={JSON.parse(resource.resource).preview.path}
                            />
                          ) : null}
                        </div>
                      </div>
                    </Row>
                  ))}
                </div>
              </div>
            ) : (
              <Typography variant="h4" gutterBottom>
                No hay nada para mostrar
              </Typography>
            )}
            <div
              //move button to rigth
              style={{
                display: "flex",
                flexDirection: "row",
                width: "100%",
                justifyContent: "flex-end",

              }}
            >  
              <Button
                  variant="contained"
                  color="primary"
                  //onClick={saveSlide}
                  onClick={handleReturn}
                  sx={{
                    marginBottom: "6px",
                  }}
                >
                  Volver
                </Button>
            </div>
          </div>
          <Dialog open={isOpenAdd} onClose={closeAdd}>
            <DialogTitle>
              Seleccione una opcion para agregar un elemento al conjunto
            </DialogTitle>
            <DialogContent
              style={{
                display: "flex",
                flexDirection: "row",
                width: "100%",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Button
                variant="contained"
                color="primary"
                onClick={() => {
                  closeAdd();
                  openListLO();
                }}
              >
                Agregar LO
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={() => {
                  closeAdd();
                }}
              >
                Agregar cuestionario
              </Button>
            </DialogContent>
          </Dialog>
          <Dialog open={isOpenListLO} onClose={closeListLO}>
            <DialogTitle>Seleccione un Learning Object</DialogTitle>
            <DialogContent>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  width: "100%",
                  height: "100%",
                }}
              >
                <FormControl>
                  <RadioGroup
                    // defaultValue={listLO ? listLO[0]._id : ""}
                    aria-labelledby="demo-radio-buttons-group-label"
                    name="radio-buttons-group"
                    onChange={handleChangeRadio}
                    value={radioB}
                  >
                    {listLO.length !== 0 ? (
                      <div style={{ width: "100%" }}>
                        {listLO.map((element) => {
                          return (
                            <div
                              key={element._id}
                              style={{
                                marginTop: "2px",
                                marginBottom: "2px",
                              }}
                            >
                              <div
                                style={{
                                  padding: "10px",
                                  display: "flex",
                                  flexDirection: "row",
                                  justifyContent: "flex-start",
                                  width: "100%",
                                }}
                              >
                                <div>
                                  <FormControlLabel
                                    value={element._id}
                                    control={<Radio />}
                                    style={{
                                      color: "white",
                                    }}
                                  />
                                </div>
                                <div key={element._id}>
                                  <IoIcons.IoIosJournal
                                    style={{
                                      fontSize: "1.5rem",
                                    }}
                                  />
                                </div>
                                <div>{element.title}</div>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    ) : (
                      <div>Aun no tiene ningun recurso</div>
                    )}
                  </RadioGroup>
                </FormControl>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    setSlide({
                      ...slide,
                      learningObjects: [
                        ...slide.learningObjects,
                        {
                          ...listLO[
                            listLO.findIndex(
                              (element) => element._id === radioB
                            )
                          ],
                          content: JSON.parse(
                            listLO[
                              listLO.findIndex(
                                (element) => element._id === radioB
                              )
                            ].content
                          ),
                        },
                      ],
                    });
                    closeListLO();
                  }}
                >
                  Agregar
                </Button>
              </div>
            </DialogContent>
          </Dialog>
          <Dialog
            open={idOpenDel}
            onClose={() => {
              navigate(`/learningObjects/slides`);
            }}
          >
            <DialogTitle>
              Este conjundo de LOs se elimino exitosamente
            </DialogTitle>
          </Dialog>

          <Dialog
              open={idOpenSave}
          >
              <Alert
                onClose={() => {
                  navigate("/learningObjects/slides");
                }}
              >
                Slide guardado exitosamente!
              </Alert>
          </Dialog>

        </div>
      )}
    </div>
  );
}

export default Slide;
