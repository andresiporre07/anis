import React, { useEffect, useState } from "react";
import services from "../../services/services";
import TYPE from "../../utilities/Constant";
import Parser from "html-react-parser";
import "react-widgets/styles.css";
import { Button, Row, Col, Form } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./LO.css";
import * as AiIcons from "react-icons/ai";
import * as IoIcons from "react-icons/io";
import Select from "react-select";
import Lista from "../../components/ResourcesList";
import useList from "../../hooks/useList";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { Delete, Edit, ExpandMore } from "@mui/icons-material";
import {
  Accordion,
  AccordionActions,
  AccordionDetails,
  AccordionSummary,
  Alert,
  Dialog,
  Fab,
  FormControl,
  Modal,
  RadioGroup,
  Stack,
  Typography,
} from "@mui/material";
import {
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from "@mui/material";
import useModal from "../../hooks/useModal";
import { useParams, useNavigate } from "react-router-dom";

const options = [
  { value: "Fisica", label: "Fisica" },
  { value: "Logica", label: "Logica" },
  { value: "Informatica", label: "Informatica" },
  { value: "Biologia", label: "Biologia" },
];
const optionsCI = [
  { value: "Auditivo", label: "Auditivo" },
  { value: "Visual", label: "Visual" },
  { value: "Motora", label: "Motora" },
  { value: "Cognitivo", label: "Cognitivo" },
];
function NewLOpage() {
  const { idLO } = useParams();
  const [list, loading, deleteElement] = useList({
    methodRequest: services.getResources,
  });
  const [loadingLO, setLoadingLO] = useState(false);

  const [openEdit, setOpenEdit] = useState(true);
  const [open, setOpen] = useState(false);

  const [radioB, setRadioB] = React.useState("female");
  const [learningObject, setLearningObject] = useState({
    _id: idLO,
    title: "",
    abstract: "",
    knowledgeArea: "Matematicas",
    relationKnowledgeArea: "",
    author: "",
    degree: "Bachiller",
    criteriaInclution: "",
    learningModel: "Constructivismo",
    license: "",
    keywords: "",
    content: [],
  });

  const navigate = useNavigate();
  const handleOpen = () => {
    if (
      learningObject.title !== "" &&
      learningObject.abstract !== "" &&
      learningObject.knowledgeArea !== "" &&
      learningObject.author !== "" &&
      learningObject.degree !== "" &&
      learningObject.learningModel !== "" &&
      learningObject.license !== "" &&
      learningObject.keywords !== ""
    )
      setOpenEdit(false);
    else alert("Por favor complete todos los campos");
    // setOpenEdit(false);
  };
  const [isOpen, openModal, closeModal] = useModal(false);
  const handleChangeRadio = (event) => {
    setRadioB(event.target.value);
  };

  const handleInputsChange = (e) => {
    setLearningObject({
      ...learningObject,
      [e.target.name]: e.target.value,
      //[e.target.className]: e.target.value,
    });
    console.log(e.target.id);
  };
  const [validated, setValidated] = useState(false);

  const handleSubmitNewResource = async (e) => {
    //get resource from list
    e.preventDefault();
    if (!learningObject.content.map((r) => r._id).includes(radioB)) {
      const resource = await services.getResourceById(radioB);
      setLearningObject({
        ...learningObject,
        content: [...learningObject.content, resource.data],
      });
    } else {
      alert("El recurso ya existe");
    }
    setOpen(false);
    closeModal();
  };
  const handleCancel = () => {
    navigate("/learningObjects");
  };
  const handleSaveLO = async (e) => {
    e.preventDefault();
    if (idLO === "new") {
      const response = await services.postLO({
        ...learningObject,
        content: JSON.stringify(learningObject.content),
      });
    } else {
      const response = await services.putLO(
        {
          ...learningObject,
          content: JSON.stringify(learningObject.content),
        },
        idLO
      );
    }
    openModal();
  };

  const requestData = async () => {
    setLoadingLO(true);
    if (idLO !== "new" && idLO !== undefined && idLO !== null) {
      const response = await services.getLOid(idLO);
      setLearningObject({
        ...response.data,
        content: JSON.parse(response.data.content),
      });
    }
    setLoadingLO(false);
  };
  useEffect(() => {
    requestData();
  }, []);

  return (
    <div
      style={{
        paddingTop: "10px",
        margin: "0 auto",
      }}
    >
      <br></br>
      {loadingLO ? (
        "Cargando..."
      ) : (
        <div>
          {openEdit ? (
            <div
              className="container"
              style={{
                margin: "0 auto",
                // paddingTop: '4rem',
                width: "700px",
                height: "650px",
              }}
            >
              <h1
                style={{
                  color: "#172B4D",
                  textAlign: "center",
                  fontFamily: "roboto",
                }}
              >
                Formulario
              </h1>
              <Form>
                <Row className="mb-3">
                  <Form.Group as={Col} controlId="formGridEmail">
                    <Form.Label>Titulo</Form.Label>
                    <Form.Control
                      type="text"
                      name="title"
                      value={learningObject.title}
                      required
                      onChange={handleInputsChange}
                      placeholder="Ingrese el Titulo"
                    />
                    <Form.Control.Feedback type="invalid">
                      Please choose a username.
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Form.Group as={Col} controlId="formGridPassword">
                    <Form.Label>Area de Conocimiento</Form.Label>
                    <Form.Select
                      required
                      defaultValue="Matematicas"
                      value={learningObject.knowledgeArea}
                      name="knowledgeArea"
                      onChange={handleInputsChange}
                    >
                      <option>Matematicas</option>
                      <option>Astronomia</option>
                      <option>Fisica</option>
                      <option>Quimica</option>
                    </Form.Select>
                  </Form.Group>
                </Row>
                <Row className="mb-3">
                  <Form.Group as={Col} controlId="formGridEmail">
                    <Form.Label>Autor</Form.Label>
                    <Form.Control
                      type="text"
                      name="author"
                      required
                      value={learningObject.author}
                      onChange={handleInputsChange}
                      placeholder="Ingrese el Autor"
                    />
                  </Form.Group>

                  <Form.Group as={Col} controlId="formGridPassword">
                    <Form.Label>Modelo de Aprendizaje</Form.Label>
                    <Form.Select
                      required
                      defaultValue="Constructivismo"
                      name="learningModel"
                      value={learningObject.learningModel}
                      onChange={handleInputsChange}
                    >
                      <option>Cognitivismo</option>
                      <option>Conductismo</option>
                      <option>Constructivismo</option>
                    </Form.Select>
                  </Form.Group>
                </Row>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlTextarea1"
                >
                  <Form.Label>Resumen</Form.Label>
                  <Form.Control
                    as="textarea"
                    name="abstract"
                    required
                    value={learningObject.abstract}
                    onChange={handleInputsChange}
                    rows={3}
                    style={{
                      resize: "none",
                    }}
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formGridAddress1">
                  <Form.Label>Relacion con otras Areas</Form.Label>
                  <Select
                    //className="basic-single"
                    name="relationKnowledgeArea"
                    classNamePrefix="select"
                    isMulti
                    isDisabled={false}
                    isLoading={false}
                    isClearable={true}
                    isRtl={false}
                    isSearchable={true}
                    options={options}
                    onChange={(selectedGroup) => {
                      setLearningObject({
                        ...learningObject,
                        relationKnowledgeArea: selectedGroup
                          .map((option) => option.value)
                          .join(","),
                      });
                    }}
                  />
                </Form.Group>
                <Form.Group className="mb-3" as={Col} controlId="formGridState">
                  <Form.Label>Criterios de Inclusion</Form.Label>
                  <Select
                    //className="basic-single"
                    name="criteriaInclution"
                    classNamePrefix="select"
                    isMulti
                    isDisabled={false}
                    isLoading={false}
                    isClearable={true}
                    isRtl={false}
                    isSearchable={true}
                    options={optionsCI}
                    onChange={(selectedGroup2) => {
                      setLearningObject({
                        ...learningObject,
                        criteriaInclution: selectedGroup2
                          .map((option) => option.value)
                          .join(","),
                      });
                    }}
                  />
                </Form.Group>

                <Row className="mb-3">
                  <Form.Group as={Col} controlId="formGridZip">
                    <Form.Label>Licencia</Form.Label>
                    <Form.Select
                      required
                      defaultValue="Seleccione"
                      name="license"
                      value={learningObject.license}
                      onChange={handleInputsChange}
                    >
                      <option>...</option>
                      <option>CC</option>
                    </Form.Select>
                  </Form.Group>
                  <Form.Group as={Col} controlId="formGridZip">
                    <Form.Label>Nivel</Form.Label>
                    <Form.Select
                      required
                      defaultValue="Bachiller"
                      name="degree"
                      value={learningObject.degree}
                      onChange={handleInputsChange}
                    >
                      <option>Bachiller</option>
                      <option>Licenciatura</option>
                      <option>Maestria</option>
                      <option>Doctorado</option>
                    </Form.Select>
                  </Form.Group>
                </Row>

                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlTextarea1"
                >
                  <Form.Label>Palabras Clave</Form.Label>
                  <Form.Control
                    as="textarea"
                    name="keywords"
                    required
                    value={learningObject.keywords}
                    onChange={handleInputsChange}
                    rows={2}
                    style={{
                      resize: "none",
                    }}
                  />
                </Form.Group>

                <Row className="mx-0">
                  <Button
                    as={Col}
                    variant="secondary"
                    className="mx-2"
                    onClick={handleCancel}
                  >
                    Cancelar
                  </Button>
                  <Button
                    type="submit"
                    onClick={handleOpen}
                    as={Col}
                    variant="info"
                  >
                    Continuar
                  </Button>
                </Row>
              </Form>
              <br></br>
            </div>
          ) : (
            <div
              className="container"
              style={{
                margin: "0 auto",
                // paddingTop: '4rem',
                width: "700px",
                height: "650px",
              }}
            >
              <h1
                style={{
                  color: "#172B4D",
                  textAlign: "center",
                  fontFamily: "roboto",
                }}
              >
                Crear Objeto de Aprendizaje
              </h1>
              <br></br>
              {learningObject.content.map((resource, index) => (
                <Row className="mb-3">
                  <div
                    sm={6}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <div>
                      {resource.type === "text" ? (
                        <p>{Parser(resource.resource)}</p>
                      ) : resource.type === "image" ? (
                        <img
                          width="200px"
                          height="200px"
                          src={JSON.parse(resource.resource).preview.path}
                        />
                      ) : null}
                    </div>
                    <AccordionActions>
                      <Fab
                        size="small"
                        color="secondary"
                        onClick={() => {
                          setLearningObject({
                            ...learningObject,
                            content: learningObject.content.filter(
                              (item) => item._id !== resource._id
                            ),
                          });
                        }}
                      >
                        <Delete />
                      </Fab>
                    </AccordionActions>
                  </div>
                </Row>
              ))}
              <Row className="mb-3">
                <Col
                  sm={6}
                  style={{
                    borderStyle: "dotted",
                    backgroundColor: "#DADADA",
                    margin: "0 auto",
                  }}
                >
                  <AccordionActions>
                    <Stack direction="row" spacing={1}>
                      <Fab
                        size="small"
                        color="info"
                        onClick={() => setOpen(true)}
                      >
                        <IoIcons.IoIosAddCircleOutline
                          style={{
                            fontSize: "1.35rem",
                          }}
                        />
                      </Fab>
                    </Stack>
                  </AccordionActions>
                </Col>
              </Row>
              <Dialog
                open={open}
                onClose={() => setOpen(false)}
                aria-labelledby="dialog-title"
                aria-describedby="dialog-description"
              >
                <DialogTitle id="dialog-title">Elegir Recurso</DialogTitle>
                <DialogContent>
                  <div
                    style={{
                      width: "100%",
                      height: "100%",
                      display: "inline",
                    }}
                  >
                    <FormControl>
                      <RadioGroup
                        defaultValue={list ? list[0]._id : ""}
                        aria-labelledby="demo-radio-buttons-group-label"
                        name="radio-buttons-group"
                        onChange={handleChangeRadio}
                        value={radioB}
                      >
                        <Lista list={list} loading={loading} />
                      </RadioGroup>
                    </FormControl>
                  </div>
                  <div>
                    <Button
                      as={Col}
                      variant="info"
                      onClick={handleSubmitNewResource}
                    >
                      Agregar
                    </Button>
                  </div>
                </DialogContent>
              </Dialog>

              <Button
                as={Col}
                variant="info"
                onClick={() => {
                  setOpenEdit(true);
                }}
              >
                Atras
              </Button>
              <Button as={Col} variant="success" onClick={handleSaveLO}>
                Guardar
              </Button>

              <Dialog
                open={isOpen}
                onClose={() => {
                  navigate("/learningObjects");
                }}
              >
                <Alert
                  onClose={() => {
                    navigate("/learningObjects");
                  }}
                >
                  Se Guardo Exitosamente!
                </Alert>
              </Dialog>
            </div>
          )}
        </div>
      )}
      <br></br>
    </div>
  );
}

export default NewLOpage;
