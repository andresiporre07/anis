import { Add, Delete, Edit } from "@mui/icons-material";
import {
  Alert,
  Button,
  CircularProgress,
  Dialog,
  Fab,
  Stack,
  Typography,
} from "@mui/material";
import React from "react";
import ListaLO from "../../components/LearningObjectList";
import { useRequest } from "../../hooks/useResponse";
import services from "../../services/services";
import { useNavigate } from "react-router-dom";
import useList from "../../hooks/useList";
import CardLO from "../../components/CardLO";
import useModal from "../../hooks/useModal";

function SlidePage() {
  const navigate = useNavigate();
  const [list, loading] = useList({
    methodRequest: services.getSlides,
  });
  const [
    loadingDel,
    errorDel,
    successDel,
    responseDel,
    statusDel,
    handleRequestDel,
  ] = useRequest({
    methodRequest: services.deleteSlide,
  });
  const [isOpen, openModal, closeModal] = useModal(false);
  return (
    // <div>holla</div>
    <div
      style={{
        background: "#FAFBFC",
        flex: "1",
        width: "100%",
        height: "100%",
        minHeight: "95vh",
        padding: "20px",
      }}
    >
      <h1
        style={{ color: "#172B4D", textAlign: "center", fontFamily: "roboto" }}
      >
        Esto son los Slides
      </h1>
      <Stack
          sx={{
            paddingTop: "1rem",
            flexDirection: "column",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            }}
          direction="row"
          spacing={2}
        >
          <Button
          variant="contained"
          color="success"
          startIcon={<Add />}
          onClick={() => {
            navigate("/learningObjects/slides/new");
          }}
          >
            Nuevo
          </Button>
        </Stack>
        
      {loading ? (
        <CircularProgress />
      ) : (
        <div
          style={{
            paddingLeft: "280px",
            paddingRight: "50px",
          }}
        >
          {list.length !== 0 ? (
            <div style={{ width: "90%" }}>
              {list.map((element) => {
                return (
                  <div
                    key={element._id}
                    style={{
                      marginTop: "30px",
                      marginBottom: "30px",
                      display: "flex",
                      justifyContent: "space-between",
                      background: "#203860",
                    }}
                  >
                    <Typography style={{padding:"10px", color:"white"}}>{element.title}</Typography>
                    <div
                      style={{padding: "10px"}}
                    >
                      <Stack direction="row" spacing={1}>
                        <Fab
                          size="small"
                          color="info"
                          onClick={() =>
                            navigate(`/learningObjects/slides/${element._id}`)
                          }
                        >
                          <Edit />
                        </Fab>
                        <Fab
                          size="small"
                          color="secondary"
                          onClick={() => {
                            handleRequestDel(element._id);
                            openModal();
                          }}
                        >
                          <Delete />
                        </Fab>
                      </Stack>
                    </div>
                  </div>
                );
              })}
            </div>
          ) : (
            <div>Aun no tiene ningun recurso</div>
          )}
        </div>
      )}
      <Dialog
        open={isOpen}
        onClose={() => {
          window.location.reload();
        }}
      >
        <Alert
          onClose={() => {
            window.location.reload();
          }}
        >
          Se elimino el Slide exitosamente!
        </Alert>
      </Dialog>
    </div>
  );
}

export default SlidePage;
