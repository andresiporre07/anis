import React from 'react';
import { Link, Outlet, useParams } from "react-router-dom";


const LayoutAudio = ({ history }) => {
    let {idResource} = useParams();
    return (
		<div>
			{/* <UploadAudio history={history} /> */}
            {idResource==='new' ? <Outlet></Outlet>:<div></div> }
		</div>
	);
};
export default LayoutAudio;
