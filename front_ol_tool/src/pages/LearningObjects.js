import React from 'react';
import {
  Alert,
  AlertTitle,
  Box,
  Button,
  CircularProgress,
  Dialog,
  Stack,
} from "@mui/material";
import useList from '../hooks/useList';
import services from "../services/services";
import CardLO from '../components/CardLO'; 

function LOpage() {
  const [list, setList] = useList({
    methodRequest: services.getLO,
  });

    return (
      <div
        style={{
          background: "#FAFBFC",
          flex: "1",
          width: "100%",
          height: "100%",
          minHeight: "95vh",
        }}
      >
        <h1
          style={{ color: "#172B4D", textAlign: "center", fontFamily: "roboto" }}
        >
          Tus Learning Objects
        </h1>
        <Stack
          sx={{
            paddingTop: "1rem",
            flexDirection: "column",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            }}
          direction="row"
          spacing={2}
        >
          <Button
            variant="contained"
            color="success"
            onClick={() => {
             // closeModal();
             window.location.href = '/learningObjects/new';
            }}
          >
            Agregar
          </Button>
        </Stack>
          <div 
            style={{ 
              paddingLeft: "280px", 
              paddingRight: "50px" 
              }}
          >
            { list.length !== 0 ? (
                  <div style={{ width: "90%" }}>
                    {list.map((element) => {
                      return (
                        <div
                          key={element._id}
                          style={{
                            marginTop: "20px",
                            marginBottom: "20px",
                          }}
                        >
                          
                          <CardLO content={element} />
                        </div>
                      );
                    })}
                  </div>
              
              ) : (<div>Aun no tiene ningun recurso</div>)
            }
          </div>
      </div>
    );
  }
  
  export default LOpage;