import { CircularProgress } from "@mui/material";
import React from "react";
import Lista from "../components/ResourcesList";
import useList from "../hooks/useList";
import services from "../services/services";
function ResourcesPage() {
  const [list, loading, deleteElement] = useList({
    methodRequest: services.getResources,
  });
  return (
    <div
      style={{
        background: "#FAFBFC",
        flex: "1",
        width: "100%",
        height: "100%",
        minHeight: "95vh",
      }}
    >
      <h1
        style={{ color: "#172B4D", textAlign: "center", fontFamily: "roboto" }}
      >
        Tus Recursos
      </h1>
      <div style={{ paddingLeft: "280px", paddingRight: "50px" }}>
        {loading ? (
          <CircularProgress />
        ) : (
          <Lista list={list} loading={loading} />
        )}
      </div>
    </div>
  );
}

export default ResourcesPage;
