import React from 'react';
import { Save } from "@mui/icons-material";
import { Alert, AlertTitle, Box, Button, CircularProgress, Dialog, Stack } from "@mui/material";
import  ReactQuill  from  "react-quill";
import  "react-quill/dist/quill.snow.css";
// import "react-quill/dist/quill.core.css";
import useResource from "../hooks/useResource";
import useModal from "../hooks/useModal";
import { Link, useParams } from "react-router-dom";
import {TYPE, MODULE} from '../utilities/Constant';

export default function TextEditor(){
  let {idResource} = useParams();
  const [resource, loading, handleResource, handleTitle, handleDescription,newResource, isEnabled] = useResource({
    idR: idResource,
    type: TYPE.TEXT,
  })
  const [isOpenModal, openModal, closeModal] = useModal(false);
  
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        paddingTop: "40px",
        flexDirection: "column",
      }}
    >
      <h2>Editor de Texto</h2>
      <br></br>
      
      <section className="section2">
				  <div>
				  	<label>Titulo: </label>
				  </div>
				  <div>
            <input
            style={{ alignSelf: "flex-start", marginBottom: "20px" }}
            type="text"
            placeholder="Titulo"
            value={resource.title}
            onChange={handleTitle}
            ></input>
				  </div>
				
				  <div>
					  <label>Descripcion: </label>
				  </div>
			  	  <div>
            <textarea
            style={{ alignSelf: "flex-start", marginBottom: "20px", width:"290px", height:"100px",resize:"none" }}
            // type="text"
            placeholder="Descripcion"
            value={resource.description}
            onChange={handleDescription}
            ></textarea>
				  </div>	
				</section>
        <div
          style={{
            maxWidth:'800px',margin:'2rem auto'
          }}  
        >
          <ReactQuill
              modules={MODULE}
              theme="snow"
              placeholder="Contenido va Aqui..."
              onChange={(e) => {
                handleResource(e);
              }}
              value={resource.resource}
            />
        </div>
        <br></br>
        <Stack spacing={2}
          direction="row"
          style={{
            display: "flex",
            justifyContent: "flex-end",
          }}
        >
          <Link 
            to="/resources"
            style={{
              textDecoration: "none",
            }}
          >
          <Button
            variant="outlined"
            color="error"
            onClick={(e) => {
              
            }}
          >
            Cancelar
          </Button>
            </Link>
          <Button
            variant="contained"
            color="primary"
            onClick={(e) => {
              e.preventDefault();
              newResource(e);
              openModal();
            }}
            startIcon={<Save />}
          >
            Guardar
          </Button>
        </Stack>
      
      <Dialog open={isOpenModal} onClose={()=>{
        closeModal()
        window.location.href = `/resources/text-editor/${resource._id}`;
        }}>
        {loading ? (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100%",
            }}
          >
            <CircularProgress color="inherit" />
          </Box>
        ) : !isEnabled() ? (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
            }}
          >
            <Alert severity="error">
              <AlertTitle>Error</AlertTitle>
              El <strong>titulo</strong> y el recurso son obligatorios
              <Button onClick={closeModal} autoFocus>
                Continuar
              </Button>
            </Alert>
          </Box>
        ) : (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
            }}
          >
            <Alert severity="success">
              <AlertTitle>Exito</AlertTitle>
              Su recurso se ha guardado con éxito!!
              <Stack
                sx={{
                  paddingTop: "1rem",
                }}
                direction="row"
                spacing={2}
              >
                <Link to="/resources" style={{ textDecoration: "none" }}>
                  <Button>Salir</Button>
                </Link>
                <Link
                  to={`/resources/text-editor/${resource._id}`}
                  style={{ textDecoration: "none" }}
                >
                  <Button
                    variant="contained"
                    color="success"
                    onClick={() => {closeModal();window.location.href = `/resources/text-editor/${resource._id}`}}
                  >
                    Continuar
                  </Button>
                </Link>
              </Stack>
            </Alert>
          </Box>
        )}
      </Dialog>
    </div>
  );
};
