import axios from "axios";

const API_URL = "https://backoltool.herokuapp.com/api";

const services = {
  getResources: async () => {
    const response = await axios.get(`${API_URL}/resource`);
    return response.data.map((resource) => ({
      ...resource,
      resource:
        resource.type === "text"
          ? resource.resource
          : JSON.parse(resource.resource),
    }));
  },
  getResourceById: async (id) => {
    const response = await axios.get(`${API_URL}/resource/${id}`);
    return response.data;
  },
  postResource: async (resource) => {
    const response = await axios.post(`${API_URL}/resource`, resource);
    return response.data;
  },
  putResource: async (resource, _id) => {
    const response = await axios.put(`${API_URL}/resource/${_id}`, resource);
    return response.data;
  },
  deleteResource: async (id) => {
    const response = await axios.delete(`${API_URL}/resource/${id}`);
    return response.data;
  },
  saveFile: async (files) => {
    const file = new FormData();
    file.append("file", files);
    const response = await axios.post(`${API_URL}/files`, file);
    return response.data;
  },
  deleteFile: async (id) => {
    const response = await axios.delete(`${API_URL}/files/${id}`);
    return response.data;
  },
  updateFile: async (files, id) => {
    const file = new FormData();
    file.append("file", files);
    const response = await axios.put(`${API_URL}/files/${id}`, file);
    return response.data;
  },
  getFile: async (url) => {
    const response = await axios.get(url, { responseType: "arraybuffer" });
    return Buffer.from(response.data, "binary").toString("base64");
  },
  getSlides: async () => {
    const response = await axios.get(`${API_URL}/linkedList`);
    return response.data;
  },
  getSlideById: async (id) => {
    const response = await axios.get(`${API_URL}/linkedList/${id}`);
    return response.data;
  },
  postSlide: async (slide) => {
    const response = await axios.post(`${API_URL}/linkedList`, slide);
    return response.data;
  },
  putSlide: async (slide, _id) => {
    const response = await axios.put(`${API_URL}/linkedList/${_id}`, slide);
    return response.data;
  },
  deleteSlide: async (id) => {
    const response = await axios.delete(`${API_URL}/linkedList/${id}`);
    return response.data;
  },
  postLO: async (lo) => {
    const response = await axios.post(`${API_URL}/learningObject`, lo);
    return response.data;
  },
  getLOid: async (id) => {
    const response = await axios.get(`${API_URL}/learningObject/${id}`);
    return response.data;
  },
  getLO: async () => {
    const response = await axios.get(`${API_URL}/learningObject`);
    return response.data;
  },
  putLO: async (lo, _id) => {
    const response = await axios.put(`${API_URL}/learningObject/${_id}`, lo);
    return response.data;
  },
  deleteLO: async (id) => {
    const response = await axios.delete(`${API_URL}/learningObject/${id}`);
    return response.data;
  },
};

export default services;
