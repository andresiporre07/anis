import { TYPE } from "./Constant";


export const getTypeLink = (type) => {
  switch (type) {
    case TYPE.TEXT:
      return "/text-editor";
    case TYPE.IMAGE:
      return "/image-editor";
    case TYPE.AUDIO:
      return "/audio-editor";
    default:
      return "/";
  }
};
